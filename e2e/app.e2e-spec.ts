import { RastreadorPage } from './app.po';

describe('rastreador App', () => {
  let page: RastreadorPage;

  beforeEach(() => {
    page = new RastreadorPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
