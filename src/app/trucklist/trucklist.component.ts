import { Component, OnInit } from '@angular/core';
import { CustomService } from '../services/custom.service';

@Component({
  selector: 'app-trucklist',
  templateUrl: './trucklist.component.html',
  styleUrls: ['./trucklist.component.css']
})
export class TrucklistComponent implements OnInit {
  results:Array<Object>;
  constructor(private csApi:CustomService) { }

  ngOnInit() {
    this.getTrucks();
  }

  getTrucks(){
    this.csApi.getTrucks2().subscribe(results =>{this.results = results});
  }

}
