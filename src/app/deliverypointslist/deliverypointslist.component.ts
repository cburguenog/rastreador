import { Component, OnInit } from '@angular/core';
import { CustomService } from '../services/custom.service';

@Component({
  selector: 'app-deliverypointslist',
  templateUrl: './deliverypointslist.component.html',
  styleUrls: ['./deliverypointslist.component.css']
})
export class DeliverypointslistComponent implements OnInit {
  results:Array<Object>;
  constructor(private csApi:CustomService) { }

  ngOnInit() {
    this.getPoints();
  }

  getPoints(){
    this.csApi.getPoints()
    .subscribe(results =>{this.results = results})
  }

}
