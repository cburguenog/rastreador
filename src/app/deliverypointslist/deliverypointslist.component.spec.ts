import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliverypointslistComponent } from './deliverypointslist.component';

describe('DeliverypointslistComponent', () => {
  let component: DeliverypointslistComponent;
  let fixture: ComponentFixture<DeliverypointslistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliverypointslistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverypointslistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
