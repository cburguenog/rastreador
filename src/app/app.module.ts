import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {FlexLayoutModule} from "@angular/flex-layout";
//Services
import { LocationService } from './services/location.service';
import { CustomService } from './services/custom.service'
//Routing
import { routing,appRoutingProvides } from './app.routing';
//Firebase oAuth
import { AngularFireModule } from 'angularfire2';
//Maps
import { AgmCoreModule } from 'angular2-google-maps/core';
//material
import { MaterialModule } from '@angular/material';
//Components
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { StatusbarComponent } from './navbar/statusbar/statusbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { DriverComponent } from './formularios/driver/driver.component';
import { TruckComponent } from './formularios/truck/truck.component';
import { DeliveryPointsComponent } from './formularios/delivery-points/delivery-points.component';
import { MainPageComponent } from './main-page/main-page.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { TrucklistComponent } from './trucklist/trucklist.component';
import { DriverlistComponent } from './driverlist/driverlist.component';
import { DeliverypointslistComponent } from './deliverypointslist/deliverypointslist.component';
import { RouteslistComponent } from './routeslist/routeslist.component';
import { RoutesComponent } from './routes/routes.component';
import { AlertsComponent } from './alerts/alerts.component';
import { ProductsComponent } from './formularios/products/products.component';
import { ProductslistComponent } from './productslist/productslist.component';

const myfirebaseConfig = {
    apiKey: "AIzaSyC3hL2ldno7FscXEo0dRNau1vFMqfYuVLo",
    authDomain: "rastreador-5607a.firebaseapp.com",
    databaseURL: "https://rastreador-5607a.firebaseio.com",
    storageBucket: "rastreador-5607a.appspot.com",
    messagingSenderId: "517262824380"
  };
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    StatusbarComponent,
    SidebarComponent,
    LoginComponent,
    RegistroComponent,
    DriverComponent,
    TruckComponent,
    DeliveryPointsComponent,
    MainPageComponent,
    LandingPageComponent,
    TrucklistComponent,
    DriverlistComponent,
    DeliverypointslistComponent,
    RouteslistComponent,
    RoutesComponent,
    AlertsComponent,
    ProductsComponent,
    ProductslistComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    AngularFireModule.initializeApp(myfirebaseConfig),
    MaterialModule.forRoot(),
    FlexLayoutModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCzPwTAUN16ff364pXxjm3CSIBWRCHkMSA'
    })
  ],
  entryComponents:[
    LoginComponent,
    RegistroComponent,
    TruckComponent,
    DriverComponent,
    DeliveryPointsComponent,
    RoutesComponent,
    ProductsComponent
  ],
  providers: [appRoutingProvides,LocationService,CustomService],
  bootstrap: [AppComponent]
})
export class AppModule { }
