import { Component, OnInit } from '@angular/core';
import { CustomService } from '../services/custom.service';

@Component({
  selector: 'app-productslist',
  templateUrl: './productslist.component.html',
  styleUrls: ['./productslist.component.css']
})
export class ProductslistComponent implements OnInit {

  results:Array<Object>;
  constructor(private csApi:CustomService) { }

  ngOnInit() {
    this.getProductsList();
  }

  getProductsList(){
    this.csApi.getProducts()
    .subscribe(result => this.results = result)
  }

}
