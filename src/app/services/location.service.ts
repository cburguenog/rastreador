import { Injectable } from '@angular/core';
import { Http,Response} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs';

@Injectable()
export class LocationService {

  constructor(private http : Http) { }

  getCoordinatesByAddress(address){
    let url =`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=AIzaSyCzPwTAUN16ff364pXxjm3CSIBWRCHkMSA`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().results)
    }

    getRoute(origin,destination):Observable<Array<Object>>{
      let url =`https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=AIzaSyCzPwTAUN16ff364pXxjm3CSIBWRCHkMSA`;
      return this.http.get(url)
      .map((response:Response)=> {return response.json().routes[0].legs[0].steps})
    }

}
