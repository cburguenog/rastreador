import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs';

@Injectable()
export class CustomService {
headers = new Headers({ 'Content-Type': 'application/json' });
options = new RequestOptions({ headers: this.headers });

  constructor(private http : Http) { }
//Trucks
    createTruck(placas,modelo,marca,noEconomico,noSeguro,descripcion){
      let url =`http://localhost:4000/vehiculos/create`;
      return this.http.post(url,{placas:placas,modelo:modelo,marca:marca,noEconomico:noEconomico,noSeguro:noSeguro,descripcion:descripcion})
        .toPromise()
        .then(response => response.json().status)
      }

    getTrucks2():Observable<Array<Object>>{
        let url =`http://localhost:4000/vehiculos/get`;
        return this.http.get(url)
        .map((response:Response) =>{
          let status = response.json().status;
          if(status == 200){
            return response.json().results;
          }else{
            return response.json().results;
          }
        })
      }
//Driver
    createDriver(name,licence,telephone,email):Observable<boolean>{
        let url ="http://localhost:4000/drivers/create";
        return this.http.post(url,{name:name,telephone:telephone,licence:licence,email:email})
        .map((response:Response)=>{
          let status = response.json().status;
          if(status){return true}else{return false}
        })
      }
    getDrivers():Observable<Array<Object>>{
        let url ="http://localhost:4000/drivers/get";
        return this.http.get(url)
        .map((response:Response) =>{
          let status = response.status;
          if(status==200){ return response.json().results}else{return []}
        });
      }
//Alerts
    getAlerts():Observable<Array<Object>>{
        let url ="http://localhost:4000/alerts/get";
        return this.http.get(url)
        .map((response:Response) =>{
          let status = response.json().status;
          if (status==200){
            return response.json().results;
          }else{
            return [];
          }
        });
      }
//Products -> Mercancia
    createProduct(name,descripcion):Observable<boolean>{
        let url="http://localhost:4000/productos/create";
        return this.http.post(url,{name:name,descripcion:descripcion})
        .map((response:Response) =>{
          let status = response.json().status;
          if(status ==200){return true;}else{ return false}
        });
      }
    getProducts():Observable<Array<Object>>{
        let url ="http://localhost:4000/productos/get";
        return this.http.get(url)
        .map((response:Response) => {
          let status =response.json().status;
          if(status==200){return response.json().results}else{return []}
        });
      }
//routes
    createRoute(name,truckId,driverId,deliveryId,productId):Observable<boolean>{
      let url ="http://localhost:4000/routes/create";
      return this.http.post(url,{name:name,truckId:truckId,driverId:driverId,deliveryId:deliveryId,productId:productId})
      .map((response:Response) => {
        let status = response.json().status;
        if(status ==200){return true;} else{return false;}
      });
    }
    getRoutes(){
      let url ="http://localhost:4000/routes/get";
      return this.http.get(url)
    }
//points
  createPoint(address,lat,lng,hours,name):Observable<boolean>{
    let url ="http://localhost:4000/points/create";
    return this.http.post(url,{address:address,lat:lat,lng:lng,hours:'',name:name})
      .map((response:Response) => {
        let status = response.json().status;
        if(status==200){return true;}else{return false;}
      });
  }
  getPoints():Observable<Array<Object>>{
    let url ="http://localhost:4000/points/get";
    return this.http.get(url)
      .map((response:Response)=>{
        let status = response.json().status;
        if(status == 200){return response.json().results}else{return []}
      })
  }
//User
  createUser(name,rol,password,nameCompany,email):Observable<boolean>{
    let url ="http://localhost:4000/users/create";
    return this.http.post(url,{name:name,rol:1,password:password,nombreEmpresa:nameCompany,email:email})
      .map((response:Response) => {
        let status = response.json().status;
        if(status == 200){return true}else{return false}
      });
  }
//Tracks
  getTracks(id):Observable<Array<Object>>{
    let url ="http://localhost:4000/track/"+id;
    return this.http.get(url)
    .map((response:Response) => {
      let status = response.json().status;
      if (status ==200){return response.json().positions}else{return [];}
    });
  }
  getTracksUsers():Observable<Array<Object>>{
    let url ="http://localhost:4000/track";
    return this.http.get(url)
    .map((response:Response)=>{
      let status = response.json().status;
      if(status == 200){return response.json().results}else {return []}
    })
  }
}
