import { Component, OnInit } from '@angular/core';
import { CustomService } from '../../services/custom.service';

@Component({
  selector: 'app-driver',
  templateUrl: './driver.component.html',
  styleUrls: ['./driver.component.css']
})
export class DriverComponent implements OnInit {

  name:string;
  lastname:string;
  telephone:string;
  email:string;
  licence:string;
  photo:any;
  constructor(private csApi:CustomService) { }

  createDriver(){
    this.csApi.createDriver(this.name + ' ' + this.lastname,this.licence,this.telephone,this.email)
    .subscribe(response =>{ })
  }

  ngOnInit() {
  }

}
