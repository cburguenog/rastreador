import { Component, OnInit } from '@angular/core';
import { CustomService } from '../../services/custom.service'

@Component({
  selector: 'app-truck',
  templateUrl: './truck.component.html',
  styleUrls: ['./truck.component.css']
})
export class TruckComponent implements OnInit {
  result :any;
  placas:   string;
  modelo:   string;
  marca :   string;
  noEconomico: string;
  noSeguro: string;
  descripcion: string;

  constructor(private cs:CustomService) { }

  ngOnInit() {
  }

  save(){
    this.cs.createTruck(this.placas,this.modelo,this.marca,this.noEconomico,this.noSeguro,this.descripcion)
    .then( result => this.result = result);
  }

}
