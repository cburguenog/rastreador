import { Component, OnInit } from '@angular/core';
import { CustomService } from '../../services/custom.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  name:string;
  descripcion:string;
  result: any;
  constructor(private csApi:CustomService) { }

  ngOnInit() {
  }

  createProduct(){
    this.csApi.createProduct(this.name,this.descripcion)
    .subscribe(result => this.result = result)
  }

}
