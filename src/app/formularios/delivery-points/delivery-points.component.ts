import { Component, OnInit } from '@angular/core';
import { LocationService } from '../../services/location.service';
import { CustomService } from '../../services/custom.service';

@Component({
  selector: 'app-delivery-points',
  templateUrl: './delivery-points.component.html',
  styleUrls: ['./delivery-points.component.css']
})
export class DeliveryPointsComponent implements OnInit {
  namePoint:string;
  lat:string;
  lon:string;
  address:string;
  response:any;
  showLoading:boolean = false;

  constructor(private lsApi:LocationService, private csApi:CustomService) { }

  ngOnInit() {
  }

  createDeliveryPoint(){
    this.showLoading = true;
    this.csApi.createPoint(this.address,this.lat,this.lon,' ',this.namePoint)
    .subscribe(result => {
      if(result){this.showLoading = false;}
    })
  }

  getCoordinates(){
    this.lsApi.getCoordinatesByAddress(this.address)
      .then(response => {this.response =  response; this.lat= response[0].geometry.location.lat;
      this.lon=response[0].geometry.location.lng;})
  }

}
