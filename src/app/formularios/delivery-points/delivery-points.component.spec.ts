import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryPointsComponent } from './delivery-points.component';

describe('DeliveryPointsComponent', () => {
  let component: DeliveryPointsComponent;
  let fixture: ComponentFixture<DeliveryPointsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryPointsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryPointsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
