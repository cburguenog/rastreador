import { Component, OnInit } from '@angular/core';
import { CustomService } from '../services/custom.service';

@Component({
  selector: 'app-routes',
  templateUrl: './routes.component.html',
  styleUrls: ['./routes.component.css']
})
export class RoutesComponent implements OnInit {
  selectedDriver:any;
  selectedtruck:any;
  selectedProduc:any;
  selectedpoints:any;
  drivers:Array<Object>;
  trucks:Array<Object>;
  producs:Array<Object>;
  points:Array<Object>;
  name:string;

  constructor(private csApi:CustomService) { }

  ngOnInit() {
    this.getDrivers();
    this.getTrucks();
    this.getProducts();
    this.getPoints();
  }

  getDrivers(){
    this.csApi.getDrivers()
    .subscribe(results => this.drivers = results)
  }

  getTrucks(){
    this.csApi.getTrucks2()
    .subscribe(results => this.trucks = results)
  }

  getProducts(){
    this.csApi.getProducts()
    .subscribe(results => this.producs = results)
  }

  getPoints(){
    this.csApi.getPoints()
    .subscribe(result => this.points=result)
  }

  createRoute(){
    this.csApi.createRoute(this.name,this.selectedtruck,this.selectedDriver,this.selectedpoints,this.selectedProduc)
    .subscribe();
  }

}
