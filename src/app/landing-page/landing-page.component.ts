import { Component, OnInit } from '@angular/core';
import { CustomService } from '../services/custom.service';
import { SebmGoogleMap, SebmGoogleMapPolyline, SebmGoogleMapPolylinePoint} from'angular2-google-maps/core';
import { LocationService } from '../services/location.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {
  lat: number = 19.415130;
  lng: number = -99.165628;
  resultado:any;
  tracking :string;
  Resresult:any;
  showAllTrucks:boolean=true;
  latA:number;
  lngA:number;
  latB:number;
  lngB:number;
  legs:Array<Object>;

  locationUser:any;
  endPoint:any;
  
  constructor(private cs:CustomService, private ls:LocationService) { }

  ngOnInit() {
    this.getTrucks();
  }

  getTrucks(){
    this.cs.getTracksUsers().subscribe(result => this.resultado = result)
  }

  showInMap(id,economico){
    this.tracking = economico;
    this.cs.getTracks(id)
    .subscribe(results =>{
      this.Resresult = results[0];
      this.latA =this.Resresult.userLatitude;
      this.lngA = this.Resresult.userLongitude;
      this.latB =this.Resresult.pointLatitute;
      this.lngB = this.Resresult.pointLongitude;
      this.locationUser =this.Resresult.userLatitude +','+this.Resresult.userLongitude;
      this.endPoint =this.Resresult.pointLatitute +','+this.Resresult.pointLongitude;
      this.ls.getRoute(this.locationUser,this.endPoint)
      .subscribe(results=>{
        this.legs=results;
        console.log(this.legs);
        this.showAllTrucks = false;
      })
      
    });
  }

}
