import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './main-page/main-page.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { DriverlistComponent } from './driverlist/driverlist.component';
import { DeliverypointslistComponent} from './deliverypointslist/deliverypointslist.component';
import { TrucklistComponent } from './trucklist/trucklist.component';
import { AlertsComponent } from './alerts/alerts.component';
import { RoutesComponent } from './routes/routes.component';
import { ProductslistComponent } from './productslist/productslist.component';



const appRoutes: Routes=[
  { path:'', component: MainPageComponent },
  { path:'home', component: LandingPageComponent},
  { path:'drivers', component: DriverlistComponent},
  { path:'points', component: DeliverypointslistComponent},
  { path:'trucks', component:TrucklistComponent },
  { path:'alerts', component:AlertsComponent },
  { path:'routes', component:RoutesComponent},
  { path:'products', component:ProductslistComponent }
];

export const appRoutingProvides: any[]=[];
export const routing=RouterModule.forRoot(appRoutes);
