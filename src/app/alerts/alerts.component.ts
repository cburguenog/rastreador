import { Component, OnInit } from '@angular/core';
import { CustomService } from '../services/custom.service';
@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.css']
})
export class AlertsComponent implements OnInit {

  alertas: Array<Object>;
  constructor(private csApi:CustomService) { }

  ngOnInit() {
    this.getAlerts();
  }
  
  getAlerts(){
    return this.csApi.getAlerts()
    .subscribe(result => this.alertas = result)
  }

}
