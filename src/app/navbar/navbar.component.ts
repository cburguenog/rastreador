import { Component, OnInit } from '@angular/core';
import { AngularFire } from 'angularfire2';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MdDialog, MdDialogRef} from '@angular/material';
import { LoginComponent } from '../login/login.component';
import { RegistroComponent } from '../registro/registro.component';
import { DriverComponent} from '../formularios/driver/driver.component';
import { TruckComponent} from '../formularios/truck/truck.component';
import { DeliveryPointsComponent} from '../formularios/delivery-points/delivery-points.component';
import { RoutesComponent } from '../routes/routes.component';
import { ProductsComponent } from '../formularios/products/products.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  authUser:any;
  provider: number;

  constructor(public af:AngularFire, private router:Router, private location: Location, public dialog: MdDialog) {
  this.af.auth.subscribe((auth) => {this.authUser = auth; if(this.authUser) {this.provider =auth.provider;}})
}
  ngOnInit() {
  }
  logOut(){
    this.af.auth.logout()
    .then(
      () =>{ this.location.replaceState('/');
            this.router.navigate(['']);
    })
  }
  //Open Dialogs
  openLoginDialog(){
    this.dialog.open(LoginComponent,{
      height: '380px',
      width: '350px',
    });
  }
  openRoutesDialog(){
    this.dialog.open(RoutesComponent,{
      height: '380px',
      width: '400px',
    });
  }
  openRegistrerDialog(){
    this.dialog.open(RegistroComponent,{
      height: '450px',
      width: '390px',
    });
  }
  openDriverDialog(){
    this.dialog.open(DriverComponent,{
      height: '450px',
      width: '390px',
    });
  }
  openTruckDialog(){
    this.dialog.open(TruckComponent,{
      height: '450px',
      width: '390px',
    });
  }
  openDeliveryPointDialog(){
    this.dialog.open(DeliveryPointsComponent,{
      height: '450px',
      width: '390px',
    });
  }
  openProductsDialog(){
    this.dialog.open(ProductsComponent,{
      height: '450px',
      width: '390px',
    });
    
  }
}
