import { Component, OnInit } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email :string;
  password:string;

  constructor(public af:AngularFire, private location: Location, private router:Router) { }

  loginGoogle(){
    this.af.auth.login({
      provider: AuthProviders.Google,
      method: AuthMethods.Popup
    }).then(
      ()=>{this.location.replaceState('/');
        this.router.navigate(['home'])
      }
    )
  }
  loginFacebok(){
    this.af.auth.login({
      provider: AuthProviders.Facebook,
      method: AuthMethods.Popup
    }).then(
      ()=>{this.location.replaceState('/');
        this.router.navigate(['home'])
      }
    )
  }
  loginTwitter(){
    this.af.auth.login({
      provider: AuthProviders.Twitter,
      method: AuthMethods.Popup
    }).then(
      ()=>{this.location.replaceState('/');
        this.router.navigate(['home'])
      }
    )
  }

  login(){
    console.log(this.email+" "+this.password)
  }
  ngOnInit() {
  }

}
