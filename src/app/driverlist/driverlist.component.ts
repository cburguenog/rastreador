import { Component, OnInit } from '@angular/core';
import { CustomService } from '../services/custom.service';

@Component({
  selector: 'app-driverlist',
  templateUrl: './driverlist.component.html',
  styleUrls: ['./driverlist.component.css']
})
export class DriverlistComponent implements OnInit {
  results: Array<Object>;

  constructor(private csApi: CustomService) { }

  getDriverList(){
    this.csApi.getDrivers()
    .subscribe(results =>{ this.results = results})
  }

  ngOnInit() {
    this.getDriverList();
  }

}
